---
layout: post
title: "Karmacracy - free market of rules"
author: "Damian Czapiewski"
citation: true
citation_author: "Czapiewski D."
date: 2023-06-23
comments: false
---

# Table of contents

* TOC
{:toc}


# What is Karmacracy

Karmacracy is a system that aims to maximize the collective happiness (the sum or the average of happiness of all people).

It works by creating incentives.

It can be implemented as a software (e.g. web or mobile application).

It doesn't ignore equality and justice because some kinds of equality and justice lead to maximizing the collective happiness.

# Correctness

Karmacracy should work better than other systems of organization because my model of the world implies that. But I might be unaware of something, so it might be wrong. But it's worth development and experimentation.

If you see a problem with this system, then don't reject it immediately because:
	1. there might exist a solution to that problem that you don't realize,
	2. and because you might have misunderstood something.

I still haven't included all information that I'd like to include here, and the article is still in the development.

# Goal

Karmacracy aims to maximize the collective happiness.

## Happiness definition

Happiness of a person is how much the current state of the world matches a person's ultimate preference (in other words: happiness is what people ultimately want)

"Ultimate" and "ultimately" means that if people want something as a result of wrong beliefs, then it doesn't count as happiness. For example, if you want a car because you think that it gives you a certain feeling, but the reality is that it won't, then the state of having that car is not what I define as happiness.

## Equality and justice

Equality and justice are important.

Equality and justice are not a top priority over anything else for people.

Example:
1. You are given two options.
2. Option 1: you have a temporary fever and everyone else is completely healthy.
3. Option 2: you have Alzheimer and everyone else has Alzheimer.
4. Option 1 is better in terms of collective happiness.
5. Option 2 is better in terms of equality.
6. Most people would prefer option 1.

# Challenges of Karmacracy

In a world without any system, there are two reasons why people don't take the actions that are beneficial for collective happiness:
1. Selfishness (they want their own happiness, not the collective happiness)
	1. Selfishness of omission - not taking an action that is beneficial for others
	2. Selfishness of commission - taking an action that is harmful for others
2. Incompetence (they don't know how actions impact the collective happiness)
	1. Incompetence of consequences - lack of the ability to predict the results of an action
	2. Incompetence of preferences - lack of knowledge of what other people prefer

Karmacracy aims to minimize the negative impact on the collective happiness arising out of the following:
1. selfishness,
2. incompetence,
3. costs introduced by the system.

# Solutions

This sections describes the features of the system that aim to solve the challenges of Karmacracy.

## Rewards

Any person can give a monetary reward to any other person, at a cost of the amount of the reward. That feature allows to facilitate exchange - one person can offer a reward to another person, and the other person can take certain action in return (e.g. giving a product, providing a service, making a change in a product and anything else). The idea of reward/exchange is nothing new, and has been used by people for ages.

Exchange/rewarding is a partial solution to the problem of selfishness and incompetence of preferences.

In this section, I'm going to assume that incompetence of consequences is solved, and we will focus on selfishness and incompetence of preferences only. Incompetence of consequences will be handled later.

Exchange is a situation where one person gives something to another person and the other person gives something back in return.

Money is something that people universally want (almost) because they can exchange it for things that other people have. Since almost everyone can find something that they need and another person has that, everyone (almost) wants money.

Let's consider the following scenario. Oliver can either take action A or action B (and not both at the same time). The following table shows what will be the impact of those actions on the happiness of other people.

| Name     | Action A | Action B |
|----------|----------|----------|
| Oliver   | +20 sm   | 0        |
| Isabella | 0        | +30 sm   |
| Harrison | 0        | 0        |
| Lily     | 0        | 0        |

As a result of the problem of selfishness, Oliver will most likely choose the action A which will result in +20 sm of collective happiness. But it's not optimal choice for the collective happiness. The optimal choice would be action B which results in +30 sm of collective happiness.

Let's assume that 1 sm is worth $1 for everyone involved. If Oliver and Isabella agree that Isabella will give between $20 and $30 to Oliver in exchange for taking action A, then it's a win-win situation for both of them.

Exchange can solve selfishness because the person or group of people that need something the most can pay money to the person who need it less in exchange for doing what they want. That makes the other person take the action that is optimal for collective happiness. If we assume that everyone needs money to the same extent, then the act of transferring money has no impact on collective happiness because one person looses money, but another person gets the same amount of money.

The only situation when exchange can't be applied to solve selfishness is when the person or group of people who need something the most doesn't have enough money to buy it. If the group is equal in terms of wealth, then the situation like that will happen only if the person (or group) has found something more valuable to them in the past.

Therefore, if we have a group of people with equal wealth, then the exchange solves the problem of selfishness.

But in unequal world, the exchange is not a complete solution to the problem of selfishness because there will be situations when the poorer person doesn't have money to buy what they need more than the rich person does.

Exchange also solves the problem of incompetence of preferences because the amount of money that users are willing to pay for something provides the information of how much value it has for the given user. For example, if a user is willing to pay at most $20 for a bottle of milk, it means that the bottle of milk gives him $20 of value.

There are two problems with exchange as a solution to selfishness problem that we need to remember:
1. Inequality - exchange doesn't work well as a solution in a world with lots of inequality.
2. Inefficiency - there is no central planning, so everyone needs to make their own decision as to who should be rewarded, so it involves lots of people's time.

There will be solutions to those problems later.

## Penalties

There are two (at least) type of penalties that can be facilitated using exchange:
1. No-reward penalties.
2. Two-sided penalties.

In Karmacracy, financial penalties are the primary way to penalize. Financial penalties have an advantage over other forms of penalization that they have an added benefits that the cost that the penalized person pays as part of being penalized can be redistributed among the society.

### No-reward penalties

No-reward penalties - a person A has the option of incentivizing people not to take a harmful action in the following way:
1. A person A periodically distributes a certain amount of money to all people who have ability to take the harmful action as a reward for not taking the harmful action.
2. When a person B takes the harmful action, the amount of the fine is deducted from their share of money that they receive as a reward.

### Two-sided penalties

Two-sided penalties work as follows.

Any person can impose a monetary penalty (fine) to any other person, at a cost of the amount of the penalty. That feature allows to deter a person / group of people from taking an action that is harmful to a person / group of people. I refer to this kind of penalty as "two-sided penalty" (since the penalizing person pays the cost themselves, so it's a penalty that harms both sides). The money that is taken is redistributed among the members of a community (or just disappears).

Being penalized with a two-sided penalty requires stacking some money in the system for some time. The incentive for a person to do that is that they receive some privilege for doing that, depending on the community. For example, if the community represents people living on the same territory (country), then the privilege might be that the person is allowed to reside on that territory (only if they have required amount of money on the stack for required amount of time).

### Impact

Penalties solve selfishness of commission problem because they deter people from taking a harmful action. The rationale behind that is similar to the one explained in reward, but in the other direction.

The penalties introduced here come with similar problems to rewards (for the same reasons):
1. Inequality
2. Inefficiency

## Policies

Any person can create a policy. Policy is a set of rules describing in what situation a reward or penalty will be imposed and what will be the amount of reward or penalty. Policy has a budget. People can vote with their money on policies. When they vote on a penalty, that money is transferred to the policy budget.

The owner of a policy (the creator, or a person assigned by the creator) can then use that budget to distribute rewards and penalties (e.g. two-sided penalties and no-reward penalties), according to the rules described in the policy. In a policy, an amount of a reward or a penalty in a specific situation doesn't need to be specified as a number, it can be specified as a formula to calculate that amount. Typically, it should be a formula depending on the policy budget. The intention behind the "policy" feature is that people will be rewarded/penalized proportionally to how much important a given policy is to people.

Not all money from the policy budget needs to be spent on rewarding or penalizing. They can be also spent for example on measuring an impact of a previous policy to increase people's knowledge of some policy. The creator of the policy can also take some fee for the service that they offer (which is inventing and managing the policy). The money can spent on virtually anything, but the owner needs to write in the description how the money will be spent. Otherwise, people won't want to vote for that policy, if they don't know how the money will be spent.

The rules described in the policy aim to serve the interest of the people who vote for the policy.

A policy can decide to reward/penalize people not only for the action that will be taken in the future, but also for past actions.

A policy also needs to specify who will be responsible for judging and enforcing the policies (it can be a person, company, group of people, and anything else). If people don't like the judge, they will vote for a different policy.

A policy can have multiple owners, if the creator choose to assign multiple owners.

Not all money in the policy budget needs to be spent on rewards and penalties. The money can be also spend on: rewarding the policy creator, measuring efficiency of the policy and anything else.

People are allowed to disobey the rules, but they simply have to pay a penalty when they do, and the system ensures that the penalty is proportional to the harm. Because sometimes a harmful action is also beneficial, and when the benefits outweigh the cost, the person should still do the action, given that we want to maximize the collective happiness.

The system also ensures that every user who needs to know about newly created (or changed) rules is informed about that (to the extent to which they choose to be informed about that).

### Conditional votes

A person can vote for a policy conditionally where the condition is the amount of money in the policy budget, e.g. they can decide that they vote $20 but only if the amount of money in the policy budget is at least $100. The vote will be executed once the policy budget (including conditional votes that can be applied) is at least $100.

Example:

Person 1 makes a conditional vote of $20 under the condition that policy budget is at least $60.

Person 2 votes $20.

Person 3 makes a conditional vote of $20 under the condition that policy budget is at least $60.

At this point, the conditional votes can be executed because if we apply all votes, we get $60 in the policy budget.

### Impact

Rewards, penalties and policies together solve the selfishness problem (with one caveat which I'll mention later). That is because when one person needs something more than the other person, then they can offer a reward or a penalty (depending if it's an action or lack of action). Presuming that both people (or groups of people) are on the same level of wealth and power, the person who needs their thing most will be the one who will offer more money. So if people are on the same level, that system will result in the optimal action being taken.

The caveat is again that the system works well as much as there is equality between people. For that reason, there must be another system alongside or on top of Karmacracy system that will solve the problem of inequality (to the right extent, since some inequality is needed in order to incentivize people to contribute value).

The above features also solve incompetence of preferences problem (not knowing the preferences of other people) because the amount of rewards and penalties provides information to other people about how much other people prefer certain states of the world or actions.

## Universal Basic Income

As explained before, the previous features work well proportionally to the equality among the group of people. For that reason, there needs to be a system that keeps the equality at the right level.

Here are few things that I'd like to say:
1. I agree that there is a need for a system that holds equality at the right level.
2. However, the right level of equality is not necessarily complete equality because if everyone was equal or whole money was redistributed among all people, then money lose its purpose and people don't have any incentive to contribute value to the group because they are not rewarded for it.
3. Probably, the current state of the world is such that there is too much inequality.
4. Other systems suffer from the inequality problem as well. The problem generally is that powerful people, by definition, have more power to decide what the future is going to be and giving up their power is not in their best selfish interest (although I expect that many people are good, so I expect that many people would be willing to vote for redistribution, even if it means they will lose money), so the future is likely to be unequal. That problem exists with other systems as well, some systems find some solution to that problem like for example in electoral democracy bribery is illegal, but if there is any solution that works well, it probably can be applied in Karmacracy as well. For example, instead of voting with money, people might receive equal number of votes to decide the policies, and trying to buy someones votes (bribery) could be penalized.

When considering the percentage of money that needs to be redistributed as part of the universal basic income, the following needs to be considered:
1. Inequality is easier to avoid than heal because once powerful people are powerful they have more power to decide the future and it's not in their selfish interest to give up their power (although I expect that many people are good, so I expect that many people would be willing to vote for redistribution, even if it means they will lose money). So, it's better to err on the side of equality.
2. In Karmacracy, money has more value because it can be used to reward and penalize (unlike in normal system where it's used only to reward). The percentage of money that needs to be redistributed needs to be therefore higher than in a normal system.

<!-- ### System of ensuring equality

Here's how a system of ensuring equality that would be above Karmacracy potentially could work (presuming that people agree to it).

1. We periodically elect a government through randomly sampling some people (to ensure that the decisions will represent all people equally) and/or electoral democracy (and possibly some other means). We should keep the number of people in the government to be a small group and not all people because:
	1. They will vote with another currency (to ensure equality) and it's easier to ensure that there is no bribery when we need to monitor a small number of people against the bribery.
	2. We don't want to involve a lot of people's time.
2. The goal of that government is to decide the policies that aim to ensure the right level of equality, since Karmacracy requires the right level of equality to get the optimal result.
3. All people in the government are educated on the subject by experts to increase their competence on the subject.
4. The government uses a system like Karmacracy with a couple of changes, to ensure the equality:
	1. People vote for policies with another special currency and at the beginning of voting, everyone has equal amount of that currency to ensure equality in decisions that will be made.
	3. In order to ensure that the government is above Karmacracy, the currency is worth X / N, where X is the complete wealth of the entire world and N is the number of people in the government. That gives the people in the government tremendous power which makes the government above anything else (except what I'm going to explain in the next point).
	4. The government can use the currency only to decide the policies relating to ensuring the right level of equality. In order to make impossible for people in the government to use their power for something else, especially their own selfish gain, people outside of the government can cancel any policy or a number of votes cast for a policy, made by people in the government. They can cancel it in the following way:
		1. Any person can request majority voting that can cancel a chosen policy or a number of votes cast for that policy, if they get a specific number of signatures.
		2. If the person collected a given number of votes, then majority voting happens, in which all people can vote (to cancel it or not).
		3. If the majority voting decides to cancel the policy or the given number of votes cast for the policy, then the policy or the given number of votes is cancelled.
5. I propose that the set of policies (rules) for ensuring equality can be as follows (this policy can be changed by the government):
	1. There is a universal basic that redistributes some percentage of money in circulation.
	2. The percentage can be decided for example by all people saying what that percentage should be and then the median of what they say will become the result.
	3. There are some other rules (policies) that ensure that the voting is fair and not manipulated in any way.

If there are some people who would lose power as a result of introducing such system, they might need to be compensated at the beginning, otherwise they might simply not agree to introducing such system. -->

## Investing in policies

Investing in policies aims to solve two problems:
1. Incompetence of consequences.
2. Inefficiency.

### Example of a problem

I'll explain those two problems on an example. Let's suppose that there is a company like Tesla that produces electric cars. There is a government that gives money to Tesla, since Tesla is doing a socially beneficial thing. Every person benefits from that a little, but there is no one person who benefits from that very much.

In Karmacracy, as described so far, someone can create a policy that is responsible for rewarding Tesla. Then people can vote for that policy with their money. But the problem is that there's no central planning, so every person will have to consider that decision separately (inefficiency problem) and everyone has to be competent to recognize that it is a good decision (incompetence of consequences problem). In that system everyone has to be competent about everything and everyone has to spend lots of time deciding what rules are good for them.

### Solution

The solution is that we need to incetivize people to vote for policies when they are good for other people, not just for them. The simplest way to incentivize people to do do that is to reward people for voting for policies that turned out to be good and penalize people for voting for policies that turned out to be bad. We can accomplish that in the following way. A person should have a way to buy a "stock" of the policy. The policy owner (a person managing the policies) can then pay dividends to the stock owners from the policy budget. In the future, when the policy turns out to be right, the policy budget will increase. The policy owner can then reward the people who turned out to be right at the beginning (who voted for the policy). If they haven't been right, then there won't be money in the policy budget. The policy owner should be transparent in the description of the policy how much money will be spent on dividends.

That relies on a premise that it's easier to say if a policy was good after the policy was in use than before it was in use. The idea behind how investing in policies solves incompetence and inefficiency problems is as follows. At the beginning a small group of competent people votes a lot of money into policies, so many people don't have to do that. Later, once the policy has been in use, a large group of people votes for that policy which makes it possible for the small group of people to get rewarded. However, after the policy was in use, it requires lower competence and less time to make the right choice, so it's not a big problem that a large group of people has to vote (although, it is still some problem that will be handled by next solutions).

Another way is that people can create a policy in the future that rewards/penalizes people who made good/bad decisions in the past. That also provides an incentive for people to vote for others (who are not competent or don't have time).

## Policy funds

Policy funds aims to solve incompetence of consequences problem.

The idea of a policy fund is that people might not know what decisions they should take as for rewarding and penalizing the members of a community, so they can pay money to a policy fund that will then use that money for them. That creates value, so the people who contributed money to the fund can receive some money back (in other words, they invest money into the fund).

The policy funds can be centralized or decentralized. A centralized policy fund is a fund where a small subset of the community members make the decisions. A decentralized policy fund is a fund when the decisions are made collectively using some system.

The application that facilitates rewards, penalties and policies might expose some API through which people can use the system programatically. That allows to create decentralized funds that can cast votes based on something that people do.

This is general idea of how a fund can operate:
1. People invest money into the fund.
2. The fund uses that money to reward people who vote correctly (possibly also penalizes the people who don't vote correctly).
3. There is an added benefit generated from solving the incompetence of consequences problem.
4. The fund gives half of the added benefit back to investors, so investors make money. The other half is the benefit that the groups gets. The fund can be rewarded for that benefit by the rules company, or the fund and rules company can be one company and then the rules company gets competitive advantage.

It's important to note that there needs to be some competition between the funds in such way that some funds impact the rules in one place and the other funds impact the rules in another place, so that people can learn which funds are good. For example, if Karmacracy is used by people living on the same territory, there should be one land applying one fund, and the other land applying another fund (at least sometimes), so that people can see the consequences of both and choose the best one.

Additionally, contribution to some fund can be potentially obligatory, for example on some territory because it's possible that some people might not realize their incompetence. However, on different territories (or in different context), should be the obligatory ones so that the funds compete for users and the best one wins.

### Competence policy fund

People can vote on a policy through a competence fund. Competence fund simply give a higher voting power to people who are more competent. The competency can be tested for example by programatically creating a list of questions about comments associated to a policy (comments are viewpoints discussing a policy, there will be more about that later, in "comments market" section) and then testing people understanding of the comments. People who understand comments well will have a higher voting power than those who don't.

### Index policy fund

Index policy fund is a fund that votes for all policies in the system proportionally to how many votes they already have.

It is easy to game that system because a person might create a policy that is good for them and they will automatically get money. However, the index policy fund can analyse the policies slightly and vote proportionally to how altruistic a policy is. If the policy has been created solely for the benefit of the creator, index fund wouldn't vote for that policy.

### Other

Other ideas how a fund can operate:
1. Simply hires a group of experts who cast votes.
2. It can use electoral democracy to hire group of experts who cast votes.
3. It can use sample a group of people who learn about a subject from experts and then cast votes (sortition).

## Prediction market

Prediction market aims to solve incompetence of consequences problem.

The idea is that there are predictions what's going to happen in the future. People can buy stock associated with a certain prediction. If the prediction comes true, then get money. If it doesn't, then they lose money. The proportion of how many people own each stock allows to learn people about what is likely to happen, therefore solving incompetence of consequences problem to an extent.

## Argument market

In argument market, people can describe their comments. Comments are represented in a tree (like on Reddit) and can be associated to policies or predictions. People can vote with money on comments which decides the visibility of the comments - the comments with the highest numbers of votes are at the top.

Of course, the visibility of the comments decided by money gives louder voice to people who have more money, but assuming right level of equality, it is good because when a person have more money, it means that they traded it for something that was more important to someone else than having that voice.

A page that displays the details of a policy or a prediction, display a tree of comments associated to that policy or prediction.

People have an incentive to vote for comments because when they think that something is true, they can take certain bets in the system (make a prediction in prediction market, cast votes on a policy and later get rewarded for that), and it's in their best interest for other people to know what they know. Additionally, the incentive for them is that they simply benefit from people making the right choices when voting for rules, so they have incentive to vote for the visibility of the best comments. 

That solves the incompetence of consequences problem (not knowing the consequences of rules).

People can take their votes back, when they think that the comments is no longer relevant or that there is no need for other people to know that comment.

Comments have a similar economy on top of them as policies, people can invest in comments and pay dividends to investors which allows to incentivize the people to make the choices that are good for collective happiness.

People are not allowed to vote against comments. It is also penalized (with a policy that can be created at the beginning) to pay someone money to take their votes back. That is because sometimes people have an incentive to hide some information, like for example if they cast votes on a policy that turned out to be bad, then they might want to hide comments that prove that the policy was bad, so that they avoid not being rewarded or even being penalized (with a reward loop feature) later.

Additionally, on any page where people can view comments, there is an AI assistant with whom people can chat. That AI assistant have the comments as context (if there are too many comments, it can use summarization, and embeddings for search) and people can chat to that AI assistant to better understand the comments of people.

## AI assistant for explanation of the system

Karmacracy might be quite complex, so there should be also an AI assistant for explaining Karmacracy to people. That AI assistant has description of Karmacracy as context and can answer people's questions about the system.

## Moderation and maintenance

The system is initially moderated by moderators. The moderators initially don't remove any content unless it is off topic. Later, the moderation rules are decided using Karmacracy.

# Evaluation

The following are the ideas on how a system of organization can be evaluated.

## Simulation

A system of organization can be evaluated using a simulation of what will happen if the system is applied. The simulation can be conducted in two ways:
1. Using a computer program - someone writes a computer program which runs a simulation, based on a certain model of the world.
2. In a person's mind - very often it's not needed to write a computer program because it is possible to predict, using a mathematical reasoning, how things are going to play out (that's how this system have been created).

The simulation can be run for many systems of organization and that way, the systems can be compared. The simulation can also help to find problems with the system.

## Games

You can divide people into groups and have them play a game that can be played in a group. The game need to be such that people in a group have their own interests (let's say they need to maximize their score in the game, each player have their own score), but the game is not a zero-sum game. Because that is how human world is - people try to maximize their happiness, but it's not a zero-sum game. The groups of people who play the game can apply different systems of organization to play the game (one group can use electoral democracy, another Karmacracy for example). The group of people that wins is likely to use the best system of organization (of course, there might other factors at play, like some groups can have better players).

The groups of people might need to play simultaneously because otherwise the groups that are tried later have an advantage since they can make use of the knowledge gained by the previous groups.

## Trying it out

The system of organization can be simply tried out to see if the model of the world that the system bases on is correct, or if there is anything missing. Of course, the system should be tried with gradually increasing the risk - at the beginning it should be tried on a small scale, if the experiment went well then it should be tried on a higher scale and so on...

## Competition

Another way to test systems of organization is to have two systems competing for the users. Users will choose being on a territory (in a metaphorical or literal sense) that is better for them.

## Measuring correlates of happiness

It's difficult to measure happiness, but we can measure some things that correlate with happiness and estimate happiness based on that. What the correlates are depends on what aspects of happiness the rules try to maximize.

## User satisfaction or increase of user satisfaction

Measuring user satisfaction might be worth it. Measuring the difference between user satisfaction after trying the system and before trying the system is even more important because as people use the system, they observe how it works.

However, the system can take some time to become good, so initial results might not be so important.

## User satisfaction among groups

I find it important to measure the ease of use and satisfaction among different groups of people (both users and potential users who didn't decide to use that application) because when one group of people have it more difficult to use the system, then they will end up discriminated since they will have to pay higher cost to participate. For example, people who can't read will definitely have a problem with participating in the system, but there's no quick and easy solution here and that problem will be present with other systems as well. The goal is to measure user satisfaction among groups and ease of use and compare it with other systems. If there's one group that favours the system more (comparing to other systems), then it is a problem of the system.

However, I don't think that measuring participation of different groups in the system is very important because it might be the result of some group of people choosing to trade the participation for something else. If a group of people prefers to spend their time on something else instead of using the system, it means that they don't need their rules as much as other groups. In a similar way, if one person goes to shop and buys butter, and another person goes to shop and buys milk, then you don't say "there's inequality of milk, so we need to take milk from the people who have it and give to the people who don't have it".

To make things clear, I find important to measure the ease of use and satisfaction among different groups of people. And I think that the discrepancies in the ease of use and satisfaction suggest that the system is bad. However, I don't think that the discrepancies in participation necessarily suggest that the system is bad.

# Applying to deciding AI rules

The process can applied to deciding AI rules in the following way:

1. AI companies need to expose themselves to be penalized (the money used for penalization might be redistributed among the companies) - they can create a group that allows people to penalize them through no-reward penalties and two-sided penalties.
2. If the system turns out to be good, AI agency should require all big AI companies to expose themselves to be penalized through the system.

# Limitations of systems

## Karmacracy

1. Inequality - the system needs another system alongside it to ensure right level or equality (e.g. universal basic income governed by a different or slightly different system). Other systems will face that problem as well in some form (I won't mention it when describing their disadvantages because I don't want to repeat myself), they can have some solutions, but those solutions most likely can be transferred to this system too.

There are also other problems, but all other problems seem to be on a good way to find a solution.

## Majority voting

1. It doesn't take into account that someone might need something more than someone else, e.g. 2 people need to apply computational power of AI to play a computer game better (not important) and 1 person needs to use AI to heal its cancer (very important). In majority voting, the 2 people will win, despite that the other choice (using AI to heal cancer) would be better for collective happiness.
2. Incompetence problem - people don't always know which choice is right.
3. Inefficiency problem - if people are expected to make the right choice, everyone needs to think long about the problem.

## Electoral democracy

1. Incompetence problem - when people elect the governors, they are not competent to make that choice.
2. Inefficiency problem - when people elect governors, they would have to spend lots of time on it to make the right choice.
3. Bribery - elected governors have a stronger incentive to serve the rich than poor since they can exchange things with rich people (unless it can be penalized and efficiently enforced)

## Sortition

1. Selfishness problem - the randomly sampled people have more power than others and might abuse that power.
2. Incompetence problem - although the randomly sampled people can be educated by experts, they might still need too much time to make the right choices.
3. Bribery - people can pay money to randomly sampled people to make certain choices.

## Liquid democracy

1. It doesn't take into account that someone might need something more than someone else, e.g. 2 people need to apply computational power of AI to play a computer game better (not important) and 1 person needs to use AI to heal its cancer (very important). In majority voting, the 2 people will win, despite that the other choice (using AI to heal cancer) would be better for collective happiness.
2. Incompetence problem - people might not realize their own incompetence and they might not realize when they should pass their vote to another person.

## Vertical voting

1. It can be used only for answering predefined questions.

# Changelog

29.06.2023 - added information about how the system for ensuring the right level of equality could work.
03.07.2023 - remove information about how the system could work because I need to slightly rethink and revise it yet.
03.07.2023 - Add "The rules described in the policy aim to serve the interest of the people who vote for the policy".
17.07.2023 - 1. Change the name "argument market" to "comment market". 2. Add information that the same economy that applies to policies applies to arguments as well. 3. Add information that contribution to some funds might be obligatory (in some context).
