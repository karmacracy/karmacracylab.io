---
layout: post
title: "List of changes to the video"
author: "Damian Czapiewski"
citation: false
date: 2023-07-03
comments: false
---

Here's the video:

[Karmacracy - free market of rules (38 minutes)](https://youtu.be/n-tMIlwdycQ)

Here are the things that I'd like to add (but you can't edit a video like you can edit a text):
1. At the end, I said that the solution to the inequality problem is universal basic income. I'd like to say that the solution is not just universal basic income, but entire system above Karmacracy that will ensure equality which might include universal basic income. The rules on how the universal basic income is organized and managed should be decided with another system as well.