---
layout: post
title: "Karmacracy - free market of rules"
author: "Damian Czapiewski"
citation: true
citation_author: "Czapiewski D."
date: 2023-07-03
comments: false
---

Karmacracy aims to maximize the collective happiness (sum or average of happiness of all people).

When reading this article, if you see a problem, please don't reject the system immediately because there might be some solution to that problem that you don't realize.

Karmacracy aims to make people take those actions that are good for the collective happiness.

So the question is: why don't people do what is good for the collective happiness?

The answers are two:

1. Selfishness (people do what maximizes their happiness, not the collective happiness).
    
2. Incompetence (people don't know what will be the impact of actions).
    
So the goal is two minimize the negative impact on the collective happiness arising out of those two, plus minimize the costs of the system (e.g. like the time involvement of people who participate in the system).

So let me talk about selfishness first, and lets assume that people are omniscient, so they will always take the right action. Under certain conditions, the optimal solution to that problem is exchange and money (which is an idea that has been used for centuries, the new stuff will be later but it's related). Let me tell you why.

Imagine that there are 4 friends: Cartman, Kyle, Stan and Kenny. Cartman can make hamburgers and Kyle and Stan would benefit from eating hamburgers. However, Cartman is lazy, so he won't make hamburgers. Presuming that Kyle and Stan want hamburgers more than the cost that Cartman pays to make hamburgers (including his money, time, effort...), that's a selfishness problem because the action that is optimal for collective happiness won't be taken.

Let's suppose that the hamburgers increase happiness of Kyle and Stan by 40 sm (sm is a unit of happiness), and making hamburgers will decrease the happiness of Cartman by 20 sm. If we assume that 1 sm is worth $1 to everyone, then Kyle and Stan can simply offer from $20 to $40 for making hamburgers, and it will be a win-win for everyone.

So, when one person need something more than the other, they can simply offer between $X and $Y, where X is the cost for a person and Y is the benefit for the other person, and that will lead to the optimal action for collective happiness being taken.

But that won't work, if the person who needs it more, doesn't have enough money to buy it. That can happen in two cases:
1. when the person has already spent their money (or something else) on something else,
2. when one person is more lucky than the other in some way - either they started with more money (or other wealth), or they have stronger ability to make impact with their actions.

As for 1, that's all right - because presuming that the person makes the right actions (we are assuming for now that people are omniscient, so they always make best actions), they have simply chosen to trade that thing for something else, so something else is more important to them.

As for 2, that's bad and that's a reason why that system won't always result with an optimal action for collective happiness being taken, so we'll need to address later.

However, if we assume a fair world - a society where luck doesn't exist and all privilege can be earned only with an effort equal to that privilege (so in that world, you can be only richer than someone else if you gave away something in the past), then exchange and money is an optimal solution to the selfishness problem.

Now, in my example with hamburgers, I focused on selfishness of omission - the problem that someone doesn't do something good because they need to pay some cost (that is lower than the benefit for others). But the selfishness of commission (doing something bad - that benefits you, but comes with a cost to others) can be solved in the same way. You can simply pay others for not doing that thing. If the cost (X) is higher than the benefit (Y), then the party that pays the cost can pay to the other party between Y and X for not taking the action and it will be a win-win for both of them.

So in other words, it is possible to penalize people simply by paying people for not taking some action. So, let's assume that a person can go to a shop during a pandemic without a mask. When they do, they create a risk for others (the cost), but they benefit from on their own. The solution is that everyone in the world (or the city, or whatever) could agree that they will pay some money periodically to all people, deducted by the cost for society that they have created by committing the harmful action (i.e. going without a mask) - that way the person will only take the action, when the benefit for collective happiness outweighs the cost (which is ideal).

However, solving selfishness of commission with exchange and money comes with the same caveat as solving selfishness of omission with exchange and money - it works fully only in a fair world (for the same reasons as with the selfishness of omission). We'll talk about that later.

Now, although in theory, that is an optimal solution in the fair world, in practice, there are simply no tools to facilitate that kind of penalization smoothly. That's where Karmacracy comes. Karmacracy is a financial service that allow people to create policies. Policy is a set of rules. Its description contains information on the situations in which a person should be rewarded or penalized, and the amount of the reward or penalty. Everyone can create a new policy. People can then vote for those policies. The money that has been voted on a policy goes to a policy budget. The owner of the policy can then spend the money in the policy budget on rewarding and penalizing people, according to the rules. That implies that the rewards and penalties are equal to the people's preferences (simplifying: an action has X cost for a group of people -> people are willing to vote X money on the policy that penalizes the action -> the policy budget is X -> the penalty is X -> a person executes the harmful action only if their benefits are higher than X -> an action is taken only if the benefit outweigh the cost).

The system is a solution to selfishness problem, in a fair world and presuming that people are omniscient.

The above system has the following limitations yet. I'm going to describe how the system addresses them:
1. Unfairness/inequality - sometimes the person who needs something more won't have money to buy it, as a result of the involvement of luck.
2. Incompetence - there's no central planning, so every user has to decide on their own which rules are worth voting for. That requires the users to be competent about everything.
3. Inefficiency - there's no central planning, so every user has to decide on their own which rules are worth voting for which requires a lot of time involvement from the users.

As for incompetence and inefficiency, there are few solutions:
1. Investing in policies - people can buy a share of a policy (in exchange for voting on a policy) and then the policy owners can pay dividends to people from the policy budget. That incentivizes people to vote for a policy when they expect that in the future the policy will turn out to be good.
2. Policy funds - people can create funds. When people don't know / don't have time to decide which policies they should vote for, then they can invest money into a fund. Funds have some way to decide which policies to vote for, and then people can get some money back because the fund creates some added value.
3. ...

As for unfairness/inequality, the system requires another system above it (or alongside it) that will solve that problem, ensuring the right level of equality and fairness. That system might involve a universal basic income (getting wealthy/powerful is a combination of effort and luck, so on average a wealthy/powerful person had more luck, so just taking right amount of money from rich and giving to the poor is beneficial for the collective happiness, especially combined with some other things that are known about the world), although universal basic income alone is not an optimal solution. However, there is a problem with ensuring fairness. The problem is that what the future is going to be is decided by powerful people to a higher extent, and it's not in the selfish interest of the unfairly privileged people to give up their power. There are some unfairly privileged people who would vote for some redistribution of power simply because they are altruistic (prioritising the collective happiness over their own), but there are also some people who are selfish. So, if we already live in an unfair world, it might be difficult to get bring the optimal level of equality. However, that problem exists in every system, so it's not an argument against the system.