---
layout: post
title: "Karmacracy - free market of rules (summary)"
author: "Damian Czapiewski"
citation: true
citation_author: "Czapiewski D."
date: 2023-06-23
comments: false
---

Karmacracy maximizes the collective happiness (it doesn't ignore equality or justice because equality or justice leads to the collective happiness).

If you see a problem with the system, don't reject it immediately because there might be some solution to it.

The key feature of Karmacracy is policies. Policy is a set of rules. Its description contains information on the situations in which a person should be rewarded or penalized, and the amount of the reward or penalty. Every user can create a policy. Policy has a budget. People can vote with their money on policies. When they vote with their money, the money is transferred to the policy budget. A user that is assigned by the policy creator is responsible for enforcing the policy using financial rewards and financial penalties that are funded by the policy budget.

Rewarding a person is straight-forward. You can simply give someone money.

Karmacracy provides two methods for penalization: no-reward penalties and two-sided penalties. The idea of both methods is that a person can financially penalize another person, at a cost that is equal or proportional to the penalty or the benefit of deterrence of a harmful action.

The rewards and penalties are funded by the policy budget. In a policy, the amount of reward or penalty in a specific situation doesn't need to be specified as a number, it can be specified as a formula to calculate that amount. Typically, it should be a formula depending on the policy budget. That implies that the rewards and penalties are proportional to how much money people voted for the given policy. That implies that the rewards and penalties are proportional to the people's preferences (simplifying: an action has X cost for a group of people -> people are willing to vote X money on the policy that penalizes the action -> the policy budget is X -> the penalty is X -> a person executes the harmful action only if their benefits are higher than X -> an action is taken only if the benefit outweigh the cost).

However, not all money needs to be spent on rewards and penalties. It can be spent for example on: rewarding the policy inventor and/or maintainer, or measuring the efficiency of the policy. The policy owner simply needs to be transparent about where the money will go.

People are allowed to disobey the rules, but they simply have to pay a penalty when they do, and the system ensures that the penalty is proportional to the cost. Because sometimes a harmful action is also beneficial, and when the benefits outweigh the cost, the person should still do the action, given that we want to maximize the collective happiness.

The system is a solution to selfishness (misalignment) problem, in an equal world.

The above system has the following limitations yet. I'm going to describe how the system addresses them:
1. Inequality - powerful people (mostly rich) will have too much power comparing to less powerful.
2. Incompetence - there's no central planning, so every user has to decide on their own which rules are worth voting for. That requires the users to be competent about everything.
3. Inefficiency - there's no central planning, so every user has to decide on their own which rules are worth voting for which requires a lot of time involvement from the users.

As for incompetence and inefficiency, there are few solutions:
1. Investing in policies - people can buy a share of a policy (in exchange for voting on a policy) and then the policy owners can pay dividends to people from the policy budget. That incentivizes people to vote for a policy when they expect that in the future the policy will turn out to be good.
2. Policy funds - people can create funds. When people don't know / don't have time to decide which policies they should vote for, then they can invest money into a fund. Funds have some way to decide which policies to vote for, and then people can get some money back because the fund creates some added value.
3. ...

As for inequality...

Firstly, it's worth noting that the problem occurs in other political systems as well. The powerful people, by definition, have a higher power to decide about what future will become, and it's not in their selfish interest to create a future in which they have less power than currently.

Secondly, Karmacracy is supposed to be used with another system that ensures the right level of equality. That system can be above Karmacracy.
