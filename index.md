---
layout: page
title: "Home"
---

Karmacracy is a system that aims to maximize the collective happiness (it doesn't ignore equality or justice because equality and justice lead to collective happiness).

The system can be useful for example for:
1. deciding rules that AI systems should follow,
2. deciding rules among shareholders and employees of a company,
3. deciding rules among other groups of people.

If you'd like to learn about it, read one of the posts explaining Karmacracy:

1. [Karmacracy - free market of rules (summary, ~5000 characters, ~8000 words)]({% link summary.md %})
2. [Karmacracy - free market of rules (summary, ~9000 characters, ~16000 words)]({% link short.md %})
3. [Karmacracy - free market of rules (full length)]({% link long.md %})

Or if you prefer a video:
1. [Karmacracy - free market of rules (38 minutes)](https://youtu.be/n-tMIlwdycQ) (note: I can't edit a video, so here's a list of things I'd change/add: [list]({% link list.md %}))

I advise against using AI tools like "Chat with PDF" to answer questions about any of the above articles. The way those tools usually work is that they give their answers based on passages of text that were taken out of context. For that reason, they often give misleading answers.

Please contact me, if you'd like:
1. use or experiment with the system,
2. contribute to the project,
3. fund the project,
4. stay updated about the project.